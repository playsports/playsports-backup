require "date"
require "yaml"
require 'active_support/all'

module PlaySports
  class Backup

    LOGGER = Logging.logger(STDOUT)
    
    def initialize(environment)
      @environment = environment
      @config = YAML.load(File.open("./config.yml"))

      Aws.config.update({
        region: 'us-east-1',
        credentials: Aws::Credentials.new(@config["aws"]["access_key"], @config["aws"]["secret"])
      })

    end

    def create_snapshot
      LOGGER.info "Fazendo snapshot da maquina"      
      ec2 = Aws::EC2::Client.new
      ec2.create_snapshot(volume_id: @config["aws"]["volumes"][@environment], description: "snapshot-#{@environment}-#{Date.today}")
      LOGGER.info "Você pode acompanhar o progresso pelo painel de controle"
    end

    def delete_older_backups

      ec2 = Aws::EC2::Client.new
      threshold = @config["aws"]["snapshot"]["threshold"]
      threshold_date = "#{threshold.day.ago}"

      
      response = ec2.describe_snapshots({filters: [
        {
          name: "volume-id",
          values: [@config["aws"]["volumes"][@environment]]
        }]
      })

      response[0].each do |snapshot|
        if Date.parse(snapshot.start_time.to_s) < Date.parse(threshold_date)
          LOGGER.info "deletando snaphot #{snapshot.snapshot_id}"
          ec2.delete_snapshot(snapshot_id: snapshot.snapshot_id)
          LOGGER.info "Snaphot #{snapshot.snapshot_id} deletado com sucesso"
        end
      end
    end
  end
end