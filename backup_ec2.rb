require "rubygems" # ruby1.9 doesn't "require" it though
require "thor"
require "rake"
require "date"
require "aws-sdk"
require "logging"
require_relative "backup"

class BackupEc2 < Thor
  
  LOGGER = Logging.logger(STDOUT)

  desc "create a snapshot from a ec2 instance", "Ex: ruby backup_ec2.rb backup [environment |production or homolog|]"
  def backup(environment)
    PlaySports::Backup.new(environment).create_snapshot
  end

  desc "delete old snapshots. The threshold is defined in config.yml.", "Ex: ruby backup_ec2.rb delete [environment |production or homolog|]"
  def delete(environment)
    PlaySports::Backup.new(environment).delete_older_backups
  end

end

BackupEc2.start